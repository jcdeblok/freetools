Welcome to the readme of the ProTrajectoryHandles script. The readme contains a list of the contents of the installer and some instructions on how to install the script.
You can install the mzp-file by dropping it onto any viewport, or by running it from the menu: MAXScript>>Run file...
If you want to check out the contents of the mzp-file yourself, you can unzip it just like a zip-file.

A message from the developer
	This will install ProTrajectoryHandles.
	License: Freeware
	Support: Jonathan de Blok
	Contact: www.jdbgraphics.nl

name "ProTrajectoryHandles"
description "ProTrajectory Handles creates viewport handles for trajectories"
version 1.6

The following files are copied to your system
	PTH.ms >> $userScripts\ProTrajectoryHandles\
	lib\PTH.ini >> $userScripts\ProTrajectoryHandles\
	lib\installed.ms >> $temp\

If dropped on the viewport, the following scripts are executed
	PTH.ms
	installed.ms

If executed by the menu: MAXScript>>Run file... the following scripts are executed 
	PTH.ms
	installed.ms

