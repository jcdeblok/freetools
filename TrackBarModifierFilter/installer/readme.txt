Welcome to the readme of the TrackBarModFilter script. The readme contains a list of the contents of the installer and some instructions on how to install the script.
You can install the mzp-file by dropping it onto any viewport, or by running it from the menu: MAXScript>>Run file...
If you want to check out the contents of the mzp-file yourself, you can unzip it just like a zip-file.

A message from the developer
	
	License: Freeware
	Support: Jonathan de Blok
	Contact: www.jdbgraphics.nl

name "trackbar_modifier_filter"
description "Show only keys from selected modifiers in the trackbar"
version 1.0

The following files are copied to your system
	trackbar_modifier_filter.ms >> $userStartupScripts\JDB_TrackBarModFilter\
	lib/installed.ms >> $temp\

If dropped on the viewport, the following scripts are executed
	trackbar_modifier_filter.ms
	installed.ms

If executed by the menu: MAXScript>>Run file... the following scripts are executed 
	trackbar_modifier_filter.ms
	installed.ms

