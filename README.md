# README #

A repo containing all my free maxscript tools.

Ready to go installers are generally found in the script's 'install' directory. 

Howto install a mzp file: In Max goto the 'scripting menu' -> 'run script' -> pick the mzp file and follow instructions.


# AVAILABLE SCRIPTS #

Houdini:  Various Houdini utils. (ProSequencer, ProNavigator)

MaxSQL:  framework to get anything from a scene in a unified SQL type way. 

ProFocus:  Click in viewport to set camera focus on that point.

ProSequencer:  Kickass camera sequencer

ProTrajactoryHandles: Powerfull in-viewport motionpath editor.

TrackBarModifierFilter: Only show animation keys in the trackbar belonging to the selected modifier in the stack.


